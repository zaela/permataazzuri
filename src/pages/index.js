import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { useState } from 'react'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const [bilangan, setBilangan] = useState([])
  const [jawaban1, setJawaban1] = useState('')
  const [jawaban2, setJawaban2] = useState('')
  const [jawaban3, setJawaban3] = useState('')
  const [jawaban4, setJawaban4] = useState('')
  const [jawaban5, setJawaban5] = useState('')
  const [jawaban6ganjil, setJawaban6ganjil] = useState('')
  const [jawaban6genap, setJawaban6genap] = useState('')
  const [jawaban7ganjil, setJawaban7ganjil] = useState('')
  const [jawaban7genap, setJawaban7genap] = useState('')

  const jumlahAnggota = (bilangan) => {
      return bilangan.length
  }

  const smallest = (bilangan) => {
      return bilangan.slice().sort((a,b)=>a-b)[0]
  }

  const biggest = (bilangan) => {
      return bilangan.slice().sort((a,b)=>a-b)[bilangan.length - 1]
  }

  const equalOrBigger = (bilangan) => {
      let bilanganAsli = []
      bilangan.forEach(number => {
          if (number >= 1) {
              bilanganAsli.push(number)
          }
      });
      return bilanganAsli
  }

  const sortAsc = (bilangan) => {
      return bilangan.sort((a, b) => { return a - b });
  }

  const ganjil = (bilangan) => {
      let bilanganGanjil = 0
      bilangan.forEach(number => {
          if (number%2) {
              bilanganGanjil +=1
          }
      });
      return bilanganGanjil
  }

  const genap = (bilangan) => {
      let bilanganGenap = 0
      bilangan.forEach(number => {
          if (number%2 == 0) {
              bilanganGenap +=1
          }
      });
      return bilanganGenap
  }

  const selectGanjil = (bilangan) => {
      let bilanganGanjil = []
      bilangan.forEach(number => {
          if (number%2) {
              bilanganGanjil.push(number)
          }
      });
      return bilanganGanjil
  }

  const selectGenap = (bilangan) => {
      let bilanganGenap = []
      bilangan.forEach(number => {
          if (number%2 == 0) {
              bilanganGenap.push(number)
          }
      });
      return bilanganGenap
  }

  
  const handleProcess = (bilangan) => {
    bilangan = bilangan.split(',')
    setJawaban1(jumlahAnggota(bilangan))
    setJawaban2(smallest(bilangan))
    setJawaban3(biggest(bilangan))
    setJawaban4(equalOrBigger(bilangan))
    setJawaban5(sortAsc(bilangan))
    setJawaban6ganjil(ganjil(bilangan))
    setJawaban6genap(genap(bilangan))
    setJawaban7ganjil(selectGanjil(bilangan))
    setJawaban7genap(selectGenap(bilangan))
  }
  return (
    <>
      <Head>
        <title>Permata Azzura Test</title>
        <meta name="description" content="Permata Azzura Test" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="bg-white h-full w-full">
        <div className='w-1/2 mx-auto mt-24 border border-gray-500 p-8 rounded-lg'>
          <div className='flex gap-3'>
            <input type="text" placeholder='example = 12,23,55,67' className='w-2/3 p-2 border border-gray-300' value={bilangan} onChange={(e) => setBilangan(e.target.value)}/>
            <button className='bg-rose-500 rounded-md text-white p-3' onClick={() => handleProcess(bilangan)}>
              process
            </button>
          </div>
          <div className='h-4'/>
          <div className='w-full'>
            <p className='text-gray-500 text-base mb-3'> jawaban no 1 = {jawaban1}</p>
            <p className='text-gray-500 text-base mb-3'> jawaban no 2 = {jawaban2}</p>
            <p className='text-gray-500 text-base mb-3'> jawaban no 3 = {jawaban3}</p>
            <p className='text-gray-500 text-base mb-3'> jawaban no 4 = {jawaban4}</p>
            <p className='text-gray-500 text-base mb-3'> jawaban no 5 = {jawaban5}</p>
            <p className='text-gray-500 text-base mb-3'> jawaban no 6 ganjil = {jawaban6ganjil}</p>
            <p className='text-gray-500 text-base mb-3'> jawaban no 6 genap = {jawaban6genap}</p>
            <p className='text-gray-500 text-base mb-3'> jawaban no 7 ganjil = {jawaban7ganjil}</p>
            <p className='text-gray-500 text-base mb-3'> jawaban no 7 genap = {jawaban7genap}</p>
          </div>
        </div>
      </main>
    </>
  )
}
